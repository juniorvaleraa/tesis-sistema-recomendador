# -*- coding: utf-8 -*-
"""
Created on Wed May  4 23:02:28 2022

@author: Ender
"""
import pandas as pd
from pickle import load


def predecir (cache,path):
    
    
    
    print("Importando files...")
    files = pd.read_csv(path,header='infer')
    print("Done.")
    
   #  print("Filtrando extensiones de archivos...")
   #  files = files.drop(files[files['filename']=='.'].index) # no files
   #  files = files.drop(files[files['mimetype']=='image/jpeg'].index)
   #  files = files.drop(files[files['mimetype']=='image/gif'].index)
   #  files = files.drop(files[files['mimetype']=='image/png'].index)
   #  files = files.drop(files[files['mimetype']=='document/unknown'  ].index) 
   #  files = files.drop(files[files['mimetype']=='application/vnd.moodle.backup'].index)
   #  files = files.drop(files[files['mimetype']=='application/xml'].index)
   #  files = files.drop(files[files['mimetype']=='application/vnd.openxmlformats-officedocument.presentationml.presentationl'].index)
   #  files = files.drop(files[files['mimetype']=='image/tiff'].index)
   #  files = files.drop(files[files['mimetype']=='image/bmp'].index)
   #  files = files.drop(files[files['mimetype']=='image/vnd.microsoft.ico'].index)
   #  files = files.drop(files[files['mimetype']=='message/rfc822'].index)
   #  files = files.drop(files[files['mimetype']=='application/x-mspublisher'].index)
   #  files = files.drop(files[files['mimetype']=='application/java-archive'].index)
   #  files = files.drop(files[files['mimetype']=='application/msaccess'].index) 
   #  files = files.drop(files[files['mimetype']=='application/x-msacces'].index)
   #  files = files.drop(files[files['mimetype']=='application/octet-stream'].index)
   #  files = files.drop(files[files['mimetype']=='text/css'].index)
   #  files = files.drop(files[files['mimetype']=='text/html'].index)
   #  files = files.drop(files[files['mimetype']=='application/x-javascript'].index)
    
    features = files.loc[:,['filesize','timecreated','timemodified','filename','filepath']]
    features=features.reset_index()
    
   #  # Concatenar caracteristicas y etiquetas (ratings)
   #  #datos=pd.concat([features,ratings], axis=1, ignore_index=True)
    
   #  #renombrar columnas como corresponde
   # # features.rename(columns = {len(features.columns)-1:'rating'}, inplace = True)
   #  features.rename(columns = {len(features.columns)-1:'filepath'}, inplace = True)
   #  features.rename(columns = {len(features.columns)-2:'filename'}, inplace = True)
   #  features.rename(columns = {len(features.columns)-3:'timemodified'}, inplace = True)
   #  features.rename(columns = {len(features.columns)-4:'timecreated'}, inplace = True)
   #  features.rename(columns = {len(features.columns)-5:'filesize'}, inplace = True)
    
    
   #  features=features.reset_index()
    
   #  # Cargar modelo
    
    model = load(open('model.pkl', 'rb'))
    # load the scaler
    scaler = load(open('scaler.pkl', 'rb'))
    
    
    x = files[['filesize','timecreated','timemodified']]
    
    x_scaled = scaler.transform(x)
    
    y_pred = model.predict(x_scaled)
    y_pred=pd.DataFrame(y_pred)
    # unir y_pred a 
    
    y = pd.concat([features,y_pred],axis=1,ignore_index=True)
    
    
     
    y.rename(columns = {len(y.columns)-1:'rating'}, inplace = True)
    y.rename(columns = {len(y.columns)-2:'direccion'}, inplace = True)
    y.rename(columns = {len(y.columns)-3:'filename'}, inplace = True)
    y.rename(columns = {len(y.columns)-4:'timemodified'}, inplace = True)
    y.rename(columns = {len(y.columns)-5:'timecreated'}, inplace = True)
    y.rename(columns = {len(y.columns)-6:'filesize'}, inplace = True)
    
    
    y.drop([0], axis=1,inplace= True)
    
    y.drop_duplicates(subset='filename',inplace=True)
    y.sort_values(by=['rating'],ascending=False, inplace=True)
    
    
    # cambiar filesize de Bytes a MB
    y['filesize']= y['filesize']/1e+6

    
    y['direccion']= y['direccion'] + y['filename']
    
    #cache = 10 #mb   
    cont=0
    datos = []


    for i in range (len(y)):
        if (y.filesize.iloc[i] + cont <= int(cache) ):
            datos.append(y.iloc[i])
        
            cont = cont + y.filesize.iloc[i]
        
        else: 
            break
        
    prediccion= pd.DataFrame(datos)
    
    return prediccion




