# -*- coding: utf-8 -*-
"""
Created on Fri May 13 09:43:52 2022

@author: Ender
"""

import pandas as pd
from pickle import load


def predecir (cache,path):
    
    
    
    print("Importando files...")
    files = pd.read_csv(path,header='infer')
    print("Done.")
    
    
    features = files.loc[:,['filesize','timecreated','timemodified','filename','filepath']]
    
    
 
    features=features.reset_index()
    
    # Cargar modelo
    
    model = load(open('model.pkl', 'rb'))
    # load the scaler
    scaler = load(open('scaler.pkl', 'rb'))
    
    
    x = files[['filesize','timecreated','timemodified']]
    
    x_scaled = scaler.transform(x)
    
    y_pred = model.predict(x_scaled)
    y_pred=pd.DataFrame(y_pred)
    # unir y_pred a 
    
    y = pd.concat([features,y_pred],axis=1,ignore_index=True)
    
    
     
    y.rename(columns = {len(y.columns)-1:'rating'}, inplace = True)
    y.rename(columns = {len(y.columns)-2:'direccion'}, inplace = True)
    y.rename(columns = {len(y.columns)-3:'filename'}, inplace = True)
    y.rename(columns = {len(y.columns)-4:'timemodified'}, inplace = True)
    y.rename(columns = {len(y.columns)-5:'timecreated'}, inplace = True)
    y.rename(columns = {len(y.columns)-6:'filesize'}, inplace = True)
    
    
    y.drop([0], axis=1,inplace= True)
    
    y.drop_duplicates(subset='filename',inplace=True)
    y.sort_values(by=['rating'],ascending=False, inplace=True)
    
    
    # cambiar filesize de Bytes a MB
    y['filesize']= y['filesize']/1e+6

    
    y['direccion']= y['direccion'] + y['filename']
    
    #cache = 10 #mb   
    cont=0
    datos = []


    for i in range (len(y)):
        if (y.filesize.iloc[i] + cont <= int(cache) ):
            datos.append(y.iloc[i])
        
            cont = cont + y.filesize.iloc[i]
        
        else: 
            break
        
    prediccion= pd.DataFrame(datos)
    
    return prediccion

