# -*- coding: utf-8 -*-
"""
Created on Fri Apr 22 09:24:42 2022

@author: Ender
"""

import pandas as pd

import predecir, predecir_v2, entrenar_modelo_V2
import entrenar_modelo
from werkzeug.utils import secure_filename



# # convertir tamaño de files a Mbytes


# Definición de la web app

from flask import Flask, request, render_template




app = Flask(__name__, template_folder='templates')
app.config["CACHE_TYPE"] = "null"  

@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r


@app.route('/', methods=("POST", "GET"))
def main():

    if request.method == 'GET':
        
       return( render_template('page1.html'))
   
    
   
    if  request.method == 'POST':
        
        
         #form_name = request.form['form']
            
        if 'form1' in request.form:
        
            size_cache = request.form['form1']
            
            if size_cache == '':
                return render_template('page4.html')
                
            path = 'final_data.csv'
            datos=predecir.predecir(size_cache,path) # retorna DF con recomendaciones que caben en la cache
        
                                
  
           # return render_template('page2.html', tables=[datos.to_html(classes='data',index=False)], header ="false")
            return render_template('page2.html', column_names=datos.columns.values, data=datos.to_dict(orient='records'))

        if 'form2' in request.files:

           
            size_cache = request.form['cache']
            
            if size_cache == '':
                return render_template('page4.html')       
           
            
            file = request.files['form2']
            print(file.filename)
            path = 'archivos_adjuntos/'+str(secure_filename(file.filename))
          #  file_name2='images/'+str(secure_filename(f.filename))
            file.save(path)
           
            datos=predecir_v2.predecir(size_cache,path) # retorna DF con recomendaciones que caben en la cache
            
            return render_template('page2.html', column_names=datos.columns.values, data=datos.to_dict(orient='records'))


        
        if 'form3' in request.files:
            
            file = request.files['form3']
            path = 'archivos_adjuntos/'+str(secure_filename(file.filename))
            file.save(path)
            
            param_modelo, time_filtrado, time_training = entrenar_modelo_V2.generar_modelo(path)             
            
            return render_template ('page3.html',param_modelo=param_modelo,time_filtrado=time_filtrado, time_training= time_training)


        

        if 'form4' in request.form:
            
            param_modelo, time_filtrado, time_training = entrenar_modelo.generar_modelo()             
            
            return render_template ('page3.html',param_modelo=param_modelo,time_filtrado=time_filtrado, time_training= time_training)


        
if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')











