# -*- coding: utf-8 -*-
"""
Created on Wed May 18 23:07:22 2022

@author: Ender
"""


#------Preprocesamiento de los datos------#

## Filtrado

# Librerías
import pandas as pd
import numpy as np
import smogn

import time

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import PowerTransformer
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.tree import DecisionTreeRegressor

from sklearn.model_selection import ShuffleSplit

from pickle import dump

from sklearn.metrics import mean_absolute_error




# Funcion para determinar el rating: conteo de viewed y downloaded
#                                    de cada file en los logs

def get_data(files,logs) :
    
    files_rep = [] # lista para almacenar los ratings
    
  #  for n in range(0,len(files)): # se recorre cada archivo
    
    for n in range(0,len(files)):
        x = files.id.iloc[n]  # se toma el id del archivo
        
        n_files= len(logs[logs.objectid == x])  # aqui se extrae de los logs, aquellos
                                                # en cuya columna objectid, tengan el id
                                               #  del archivo n, y se cuentan las filas (con len) 
                                               #  y eso representa la cantidad de veces que 
                                               #  aparece ese archivo en los logs, es decir el rating
                                               
      
        files_rep.append(n_files) # vamos añadiendo el rating de cada archivo 
        print("numero añadido:", n_files) # rating resultante del archivo n
        print("Archivos procesados: ", len(files_rep) )
        
    return files_rep # lista con rating de todos los archivos


#---- Funcion para el filtrado de los datos -------#

def filtrado (path): 
    
    # logs
    print("Importando logs...")
    logs = pd.read_csv("mdl_logstore_standard_log.csv",header='infer')
    print("Done.")
    
    # eliminar rows con nan en objectid
    logs = logs[logs['objectid'].notna()] 
    
    # files
    
    print("Importando files...")
    files = pd.read_csv("mdl_files.csv",header='infer')
    print("Done.")
    
    
    # logs
    # ver las diferentes "actions"  que hay
    #logs.action.unique()
    
    
    # filtrar y dejar solo viewed y downloaded
    print("Filtrando actions de los logs (downloaded y viewed)")
    logs_filt = logs[(logs['action']=='downloaded') | (logs['action']=='viewed')]
    
    # verificar
    #logs_filt.action.unique()
    
    # Guardar logs filtrados ( en caso que se quieran aparte en un csv)
    # logs_filt.to_csv('log_filtrados.csv')
    
    #---------------------------------------------------------------
    # Filtrar y dejar solo los archivos de interés

   
    # los siguientes son tipos de archivos que no nos interesan

    print("Filtrando extensiones de archivos...")
    files = files.drop(files[files['filename']=='.'].index) # no files
    files = files.drop(files[files['mimetype']=='image/jpeg'].index)
    files = files.drop(files[files['mimetype']=='image/gif'].index)
    files = files.drop(files[files['mimetype']=='image/png'].index)
    files = files.drop(files[files['mimetype']=='document/unknown'  ].index)
    files = files.drop(files[files['mimetype']=='application/vnd.moodle.backup'].index)
    files = files.drop(files[files['mimetype']=='application/xml'].index)
    files = files.drop(files[files['mimetype']=='application/vnd.openxmlformats-officedocument.presentationml.presentationl'].index)
    files = files.drop(files[files['mimetype']=='image/tiff'].index)
    files = files.drop(files[files['mimetype']=='image/bmp'].index)
    files = files.drop(files[files['mimetype']=='image/vnd.microsoft.ico'].index)
    files = files.drop(files[files['mimetype']=='message/rfc822'].index)
    files = files.drop(files[files['mimetype']=='application/x-mspublisher'].index)
    files = files.drop(files[files['mimetype']=='application/java-archive'].index)
    files = files.drop(files[files['mimetype']=='application/msaccess'].index)
    files = files.drop(files[files['mimetype']=='application/x-msacces'].index)
    files = files.drop(files[files['mimetype']=='application/octet-stream'].index)
    files = files.drop(files[files['mimetype']=='text/css'].index)
    files = files.drop(files[files['mimetype']=='text/html'].index)
    files = files.drop(files[files['mimetype']=='application/x-javascript'].index)


    print("Las extensiones restantes son: ")
    print(files.mimetype.unique())
    

   # guardar files filtrados en un csv
    files.to_csv('files_filtrados.csv')

   
   # llamada a funcion para calcular los ratings
   
    ratings = get_data(files, logs_filt) # lista
    ratings = pd.DataFrame(ratings)      # convertir a DataFrame
    
    # Extraer campos de interés (columnas) de los archivos
    features = files.loc[:,['filesize','timecreated','timemodified','filename','filepath']]
    
    
    
   # Concatenar caracteristicas y etiquetas (ratings)
    datos=pd.concat([features,ratings], axis=1, ignore_index=True)

    #renombrar columnas como corresponde
    datos.rename(columns = {len(datos.columns)-1:'rating'}, inplace = True)
    datos.rename(columns = {len(datos.columns)-2:'filepath'}, inplace = True)
    datos.rename(columns = {len(datos.columns)-3:'filename'}, inplace = True)
    datos.rename(columns = {len(datos.columns)-4:'timemodified'}, inplace = True)
    datos.rename(columns = {len(datos.columns)-5:'timecreated'}, inplace = True)
    datos.rename(columns = {len(datos.columns)-6:'filesize'}, inplace = True)
    
    
    files_adic = pd.read_csv(path,header='infer')
    files_adic = files_adic.loc[:,['filesize','timecreated','timemodified','filename','filepath','rating']]
    
    
    
    datos_f=pd.concat([datos,files_adic], axis=0, ignore_index=True)
    # Eliminar archivos con rating 0
    
    final_data = datos_f[datos_f['rating']!=0 ]
    final_data=final_data.dropna()
    #Guardar datos en un csv
    print("Guardando data final...")
    final_data.to_csv("final_data.csv", index=False)
    print ("Data guardada.")
    #---------------------------------------#

    return final_data
  
# ejecutar funcion principal

def entrenamiento(data):
     
     # separar rating = 1 de los demás para subsampling

    data1=data[data['rating']==1]
    data2 = data[data['rating']!=1]
    
    # se toman 1000 muestras de data1
    data1=data1.sample(1000, random_state=100, axis=0) 
    
    # Data final
    data_f=pd.concat([data1, data2])
    
    # features
    x = data_f[['filesize','timecreated','timemodified']]
     
    # label
    y =data_f['rating']

    # Split data
    X_train, X_test, y_train, y_test = train_test_split(x,y, test_size=0.2,random_state=700,shuffle=True)

    X_train=pd.DataFrame(X_train)
    y_train=pd.DataFrame(y_train)


  #  Configuración para Smogn
    D=pd.concat([X_train,y_train], axis=1, ignore_index=False)
    D.reset_index(drop=True, inplace=True)
    #D = D.dropna()
    
    ## specify phi relevance values
    rg_mtrx = [

    [1,  0, 0],  ## under-sample ("majority")
    [10, 0, 0],  ## under-sample ("majority")
      
   # over-sample
    [50, 1, 0],
    [100, 1, 0],
    [200, 1, 0],
    [300, 1, 0],
    [400, 1, 0],
    [500, 1, 0],  
    [600, 1, 0],
     # [700, 1, 0],
    ]


    X_s = smogn.smoter(
    
    ## main arguments
    data = D,           ## pandas dataframe
    y = 'rating',          ## string ('header name')
    k = 7,#10,   #7 #15                ## positive integer (k < n)
    pert = 0.04, #0.04              ## real number (0 < R < 1)
    samp_method = 'balance',  ## string ('balance' or 'extreme')
    drop_na_col = True,       ## boolean (True or False)
    drop_na_row = True,       ## boolean (True or False)
    replace = False,          ## boolean (True or False)

    ## phi relevance arguments 
    rel_thres = 0.8,   #0.9    ## real number (0 < R < 1)
    rel_method = 'manual',    ## string ('auto' or 'manual')
   # rel_xtrm_type = 'both', ## unused (rel_method = 'manual')
   # rel_coef = 0.8,#1.50,        ## unused (rel_method = 'manual') # 1.50
    rel_ctrl_pts_rg =  rg_mtrx  ## 2d array (format: [x, y])
    )
    
    # Preparar para el entrenamiento
    
    X_train = X_s.drop(columns=['rating'])
    y_train = X_s[['rating']]

    #D=0
    #X_s=0
    
    print("Escalando datos...")
    scaler=  StandardScaler()
    scaler.fit(X_train)
    X_train=scaler.transform(X_train)
    X_test= scaler.transform(X_test)
    
    y_train= np.array(y_train)
    y_train=y_train.squeeze() # array 1-D

    #librerias para entrenar los modelos


   # cross validation, para evitar sobreajuste

    cv = ShuffleSplit(n_splits=3, test_size=0.2, random_state=100) # 3 carpetas o particiones
    
    # Modelos - algoritmos

    tree = DecisionTreeRegressor()
    randomfo = RandomForestRegressor(random_state=10)
    adaboost = AdaBoostRegressor(random_state=600)
    
    # Parámetros a variar de cada modelo
    parameters6 = {'estimador6__max_depth': [5,10,20,25], 'estimador6__n_estimators':[3,5,10,20]}
    parameters8 = {'estimador8__n_estimators': [3,5,10,20,50,60,80,100], 'estimador8__max_depth':[3,5,10,20]}
    parameters9 = {'estimador__n_estimators': [2,3,4,5,10,15], 'estimador__base_estimator':[RandomForestRegressor()]}
    


    # pipeline (ensamble o flujo de los pasos para desarrollar el modelo)

    pipeline = Pipeline(steps=[ # hacer que los datos tengan una
                                                                # distribución normal
                           ('transformer',PowerTransformer()), 
                        #   ('estimador',svr1)
                          # ('estimador2',ridge2),
                        # ('estimador3',knn3)
                          # ('estimador4',baye)
                          # ('estimador5',tree)
                          #  ('estimador6',randomfo)
                          #  ('estimador7',percep)
                       #   ('estimador8',grad_reg)
                           ('estimador',adaboost)
                           ],verbose=3)


    clf = GridSearchCV(pipeline, parameters9,cv=cv, scoring='neg_mean_absolute_error',verbose=3)
    #
    #mean_absolute_error
    clf.fit(X_train, y_train)
    
    
   # mostrar resultados
   # print('Best MAE: %.3f' % clf.best_score_)
   # print('Best Config: %s' % clf.best_params_)


    #guardar modelo y el escaler

    #modelo
    dump(clf, open('model.pkl', 'wb'))
    # escaler
    dump(scaler, open('scaler.pkl', 'wb'))
    
    
    # Guardar resultados del gridsearch

   # resumen= pd.DataFrame(clf.cv_results_)
   # resumen.to_csv('resumen_training.csv',index=False)
   
    # Predecir en los datos de validacion
   
    y_pred=clf.best_estimator_.predict(X_test)
    #y_pred=clf.best_estimator_.predict(X_test)
    #y_pred= modelo.predict(X_test)
    print("MAE validation data: ",mean_absolute_error(y_test, y_pred))


    return clf.best_params_



def generar_modelo (path):
    
    print("Realizando filtrado de los datos...")
    inicio = time.time()
    datos=filtrado(path)
    fin = time.time()
    time_filtrado = (fin-inicio)/60
    time_filtrado = round(time_filtrado, 2)
    
    print("Entrenando el modelo...")
    inicio = time.time()
    parametros_modelo= entrenamiento(datos)
    fin = time.time()
    time_training = (fin-inicio)/60
    time_training = round(time_training, 2)
    
    print("Entrenamiento finalizado")
    

    return parametros_modelo, time_filtrado, time_training
# Ejecutar funcion principal




    